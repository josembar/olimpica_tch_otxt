
with Divulgado(DataID) as
(select DT.DataID
-- DT.Name
from csadmin.DTree DT
where DT.ParentID = 98795 -- DataID Carpeta 02 Documentos Divulgados
union all
select p.DataID
-- p.Name
from csadmin.DTree as p, Divulgado
where p.ParentID = Divulgado.DataID)
select Divulgado.DataID
from Divulgado
inner join csadmin.LLAttrData as Codigo
on Divulgado.DataID = Codigo.ID
and Codigo.AttrID = 6
and Codigo.DefID = 96725 -- Categoría Documento Proceso, posiblemente se deba cambiar
where Codigo.ValStr = 'DDD000' --Valor a capturar desde el bws