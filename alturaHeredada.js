

/*
EIM Developer: José Barrantes
función para calcular la altura en las celdas de la columna tipo documental
la altura de cada celda de tipo documental se le va asignando a las celdas
de tipo de soporte físico y/o electrónico
*/
$(document).ready(function(){
    for (var i = 0; i < $(".tipoDocumental").size(); i++ )
    {
        var alturaHeredada = $(".tipoDocumental").eq(i).height();
        $(".soporteFisico").eq(i).css("height",alturaHeredada);
        $(".soporteElectronico").eq(i).css("height",alturaHeredada);
    }
});
