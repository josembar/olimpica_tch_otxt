-- original
-- primera exc 00:05:17
-- segunda exc 00:05:24

WITH FD(DataID,SubType,Name) AS
(SELECT DT.DataID, DT.SubType, DT.Name
FROM csadmin.DTree DT
WHERE ABS(DT.ParentID) = 181288
UNION ALL
SELECT p.DataID, p.SubType, p.Name
FROM csadmin.DTree as p, FD
WHERE ABS(p.ParentID) = FD.DataID)
SELECT DataID
FROM FD
WHERE SubType = 0 AND Name = 'FACTURA DE COMPRA MERCANCIA Y SERVICIO RETAIL Y NO RETAIL' 

-- se elimina abs
-- primera exc 00:00:00
-- segunda exc 00:00:00

WITH FD(DataID,SubType,Name) AS
(SELECT DT.DataID, DT.SubType, DT.Name
FROM csadmin.DTree DT
WHERE DT.ParentID = 181288
UNION ALL
SELECT p.DataID, p.SubType, p.Name
FROM csadmin.DTree as p, FD
WHERE p.ParentID = FD.DataID)
SELECT DataID
FROM FD
WHERE SubType = 0 AND Name = 'FACTURA DE COMPRA MERCANCIA Y SERVICIO RETAIL Y NO RETAIL' 

-- se reemplaza abs por in
-- primera exc 00:00:00
-- segunda exc 00:00:01

WITH FD(DataID,SubType,Name) AS
(SELECT DT.DataID, DT.SubType, DT.Name
FROM csadmin.DTree DT
WHERE DT.ParentID in (181288,-181288)
UNION ALL
SELECT p.DataID, p.SubType, p.Name
FROM csadmin.DTree as p, FD
WHERE p.ParentID in (FD.DataID,-FD.DataID))
SELECT DataID
FROM FD
WHERE SubType = 0 AND Name = 'FACTURA DE COMPRA MERCANCIA Y SERVICIO RETAIL Y NO RETAIL' 

-- se reemplaza abs por case
-- primera exc 00:00:01
-- segunda exc 00:00:02

WITH FD(DataID,SubType,Name) AS
(SELECT DT.DataID, DT.SubType, DT.Name
FROM csadmin.DTree DT
where (
case DT.SubType   
when 31066 then -DT.DataID
when 848 then -DT.DataID
else DT.DataID
end
) = 181288
UNION ALL
SELECT p.DataID, p.SubType, p.Name
FROM csadmin.DTree as p, FD
WHERE p.ParentID = FD.DataID)
SELECT DataID
FROM FD
WHERE SubType = 0 AND Name = 'FACTURA DE COMPRA MERCANCIA Y SERVICIO RETAIL Y NO RETAIL'