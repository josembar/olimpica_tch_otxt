with Docs ([DataID],[Name],[Level],[SubType])
		as
		(
			select 
				(case p.SubType 
				when 848 then -p.DataID 
				else p.DataID 
                		end) as DataID,
				p.Name,
				0 as [Level],
				p.SubType
			from 
				csadmin.DTree p
			-- where p.DataID = 2000 -- Busca en TODO el Content, excluye zona personal
			where p.DataID = %2 -- DataID carpetas zona empresa
				and p.Deleted = 0
			union all
			select 
				c.DataID,
				c.Name,
				[Level] + 1,
				c.SubType
			from 
				[csadmin].[DTree] c ,Docs d 
		    where d.DataID =  c.ParentID 
		          and c.Deleted = 0

		)
		select Docs.*,
		Permisos.RightID 'ID del Usuario',
		-- Personas.Name 'Nombre del Usuario/Grupo',
		Personas.Type 'TipoKUAF',
		Permisos.Permissions
		from Docs, csadmin.DTreeACL Permisos, csadmin.KUAF Personas
		where Docs.DataID = Permisos.DataID
		and Permisos.RightID = Personas.ID
		and Docs.SubType in('0','144','848')
		and Personas.ID = %1 --parametro a ingresar