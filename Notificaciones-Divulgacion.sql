
--- Extraer los ID de los usuarios que pertenecen a un grupo

select KFC.ChildID as Usuarios   
from csadmin.KUAFChildren as KFC
inner join csadmin.KUAF as KF
on KF.ID = KFC.ChildID
and KF.Type = 0
where KFC.ID = 190190 -- se debe cambiar por ID del grupo de Olímpica

--- Consultar correo de la Lista/Grupo
select Correo
from csadmin.Z_Listas_Grupos_Correo
where Lista_Grupo = %1 -- parámetro que se obtiene del atributo Lista/Grupo del WF

--- Consultar el documento divulgado de acuerdo a una gestión

--- Utilizando varios atributos como parámetros
select 
Documento.DataID,
Documento.Name
from csadmin.DTree as Documento
inner join csadmin.LLAttrData as Proceso
on Documento.DataID = Proceso.ID
and Proceso.AttrID = 2
and Proceso.DefID = 96725 --- Categoría Documento Proceso, posiblemente se deba cambiar
inner join csadmin.LLAttrData as Subproceso
on Documento.DataID = Subproceso.ID
and Subproceso.AttrID = 3
and Subproceso.DefID = 96725
inner join csadmin.LLAttrData as Tipo
on Documento.DataID = Tipo.ID
and Tipo.AttrID = 4
and Tipo.DefID = 96725
inner join csadmin.LLAttrData as Titulo
on Documento.DataID = Titulo.ID
and Titulo.AttrID = 5
and Titulo.DefID = 96725
inner join csadmin.LLAttrData as Codigo
on Documento.DataID = Codigo.ID
and Codigo.AttrID = 6
and Codigo.DefID = 96725
where Proceso.ValStr = 'Gestión Administrativa' --Valor a capturar desde el bws
and Subproceso.ValStr = 'Gestión Humana' --Valor a capturar desde el bws
and Tipo.ValStr = 'Política' --Valor a capturar desde el bws
and Titulo.ValStr = 'Prueba día 31 "Brujitas"' --Valor a capturar desde el bws
and Codigo.ValStr = 'PRUB 00031' --Valor a capturar desde el bws

--- Utilizando solo el atributo Código
select 
Documento.DataID,
Documento.Name
from csadmin.DTree as Documento
inner join csadmin.LLAttrData as Codigo
on Documento.DataID = Codigo.ID
and Codigo.AttrID = 6
and Codigo.DefID = 96725
where Codigo.ValStr = 'PRUB 00031' --Valor a capturar desde el bws


