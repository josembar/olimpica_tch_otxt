
--total radicados
select RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA Usuario,
RadicadoGenerado.FECHA_HORA_GENERADO 'Fecha_Radicado',
RadicadoGenerado.RADICADO_GENERADO 'Codigo_de_Radicado',
SegmentoReal.NOMBRE Segmento,
(case RadicadoGenerado.IMPRESO
when 1 then 'Impreso'
else 'No Impreso'
end) Estado
from CO_RADICADO_GENERADO RadicadoGenerado
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignadoUsuario
on RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO = RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO
inner join CO_SEGMENTO_GENERADO_VALOR SegmentoGeneradoValor
on SegmentoGeneradoValor.ID_RAD_GENERADO = RadicadoGenerado.ID_RAD_GENERADO
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentoGeneradoValor.ID_SEG_REAL
where SUBSTRING(SegmentoReal.NOMBRE,1,2) = 'FR'
and SUBSTRING(SegmentoReal.NOMBRE,1,6) in (select * from STRING_SPLIT ('FR_BAQ,FR_MED', ','))
--and RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA in (select * from STRING_SPLIT ('86916', ','))
--and RadicadoGenerado.FECHA_HORA_GENERADO >= CONVERT(DATE,'2018-07-30 00:00:00.000')
--and RadicadoGenerado.FECHA_HORA_GENERADO >= CONVERT(DATETIME,'2019-04-23')
--and RadicadoGenerado.FECHA_HORA_GENERADO < DATEADD(DAY,1,CONVERT(DATE,'2018-08-16 00:00:00.000'))
--and RadicadoGenerado.FECHA_HORA_GENERADO < DATEADD(DAY,1,CONVERT(DATETIME,'2019-04-23'))
order by RadicadoGenerado.FECHA_HORA_GENERADO


--total radicados por usuario
select TotalRadicados.Usuario,
TotalRadicados.Total_Radicados
from
(select ID_CONTENT Usuario,
(select count(RadicadoGenerado.RADICADO_GENERADO) 
from CO_RADICADO_GENERADO RadicadoGenerado
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignadoUsuario
on RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO = RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO
inner join CO_SEGMENTO_GENERADO_VALOR SegmentoGeneradoValor
on SegmentoGeneradoValor.ID_RAD_GENERADO = RadicadoGenerado.ID_RAD_GENERADO
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentoGeneradoValor.ID_SEG_REAL
where SUBSTRING(SegmentoReal.NOMBRE,1,2) = 'FR'
and RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA = ID_CONTENT
and SegmentoReal.NOMBRE in (select * from STRING_SPLIT ('FR_BAQ,FR_BOG', ',') )
--and RadicadoGenerado.FECHA_HORA_GENERADO >= CONVERT(DATETIME,'2018-09-13')
--and RadicadoGenerado.FECHA_HORA_GENERADO < DATEADD(DAY,1,CONVERT(DATETIME,'2019-05-02'))
) 'Total_Radicados'
from CO_USUARIO) TotalRadicados
where TotalRadicados.Total_Radicados > 0
--and ID_CONTENT in (select * from STRING_SPLIT ('86916,86256', ','))

--total radicados por segmento
select segmentos.*,
(select COUNT(SegmentoGeneradoValor.ID_RAD_GENERADO) 
from CO_SEGMENTO_GENERADO_VALOR SegmentoGeneradoValor
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentoGeneradoValor.ID_SEG_REAL
inner join CO_RADICADO_GENERADO RadicadoGenerado
on RadicadoGenerado.ID_RAD_GENERADO = SegmentoGeneradoValor.ID_RAD_GENERADO
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignadoUsuario
on RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO = RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO
where SegmentoReal.NOMBRE = Segmento
--and RadicadoGenerado.FECHA_HORA_GENERADO >= CONVERT(DATETIME,'2018-09-13')
--and RadicadoGenerado.FECHA_HORA_GENERADO < DATEADD(DAY,1,CONVERT(DATETIME,'2019-05-02'))
--and RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA in (select * from STRING_SPLIT ('86916,135177', ','))
) 'Total_Radicados'
from 
(select NOMBRE Segmento
from CO_SEGMENTO_REAL 
where SUBSTRING(NOMBRE,1,2) = 'FR'
) segmentos
where 1 = 1
--and segmentos.Segmento in (select * from STRING_SPLIT ('FR_BAQ,FR_BOG', ','))
