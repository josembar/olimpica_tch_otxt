select tabla.ID_del_Workflow,
tabla.Ruta_Almacenamiento,
tabla.Responsable_del_Tramite,
tabla.Fecha_Inicio,
tabla2.Numero_de_Radicado,
tabla2.Fecha_de_Radicado,
tabla2.Dependencia_Competente
from(select Workflow.SubWork_WorkID 'ID_del_Workflow',
(case 
when Attributes.WF_ValInt is null then 0
else 1
end) 'Ruta_Almacenamiento',
--Attributes.WF_ValInt 'Ruta_de_Almacenamiento',
Attributes2.WF_ValInt 'Responsable_del_Tramite',
Workflow.SubWork_DateInitiated 'Fecha_Inicio',
(select DataID
from DTree 
where ParentID = (select DataID from DTree where Name = CONCAT(Workflow.SubWork_Title,'-',Workflow.SubWork_WorkID))) Documento
from WSubWork Workflow
inner join WFAttrData Attributes
on Attributes.WF_ID = Workflow.SubWork_WorkID
inner join WFAttrData Attributes2
on Attributes2.WF_ID = Workflow.SubWork_WorkID
where Workflow.SubWork_Status = 1
and Attributes.WF_AttrType = 305
and Attributes2.WF_AttrType = 14) tabla
inner join 
(select Document.DataID Documento2,
AttributesCat1.ValStr 'Numero_de_Radicado',
AttributesCat2.ValDate 'Fecha_de_Radicado',
AttributesCat3.ValStr 'Dependencia_Competente'
from DTree Document
inner join LLAttrData AttributesCat1
on AttributesCat1.ID = Document.DataID 
inner join CatRegionMap CategoryName1
on CategoryName1.CatID = AttributesCat1.DefID
inner join LLAttrData AttributesCat2
on AttributesCat2.ID = Document.DataID 
inner join CatRegionMap CategoryName2
on CategoryName2.CatID = AttributesCat2.DefID
inner join LLAttrData AttributesCat3
on AttributesCat3.ID = Document.DataID 
inner join CatRegionMap CategoryName3
on CategoryName3.CatID = AttributesCat3.DefID
where CategoryName1.CatName = 'Comunicaciones Recibidas Fisicas'
and CategoryName2.CatName = CategoryName1.CatName
and CategoryName3.CatName = CategoryName1.CatName
and CategoryName1.AttrName = 'Número de Radicado'
and CategoryName2.AttrName = 'Fecha de Radicado'
and CategoryName3.AttrName = 'Dependencia Competente'
and AttributesCat2.ID = AttributesCat1.ID
and AttributesCat1.AttrID = 3
and AttributesCat2.AttrID = 4
and AttributesCat3.AttrID = 22) tabla2
on tabla2.Documento2 = tabla.Documento
where 1 = 1
--and tabla2.Numero_de_Radicado = 'CR-BAQ00000001'
--and tabla2.Dependencia_Competente = 'Dirección Nacional Fruver'
--and tabla2.Fecha_de_Radicado >= CONVERT(datetime,'2019-04-03')
--and tabla2.Fecha_de_Radicado < DATEADD(DAY,1,CONVERT(DATE,'2019-04-03'))
--and tabla.Fecha_Inicio >= CONVERT(DATE,%6,103) 
--and tabla.Fecha_Inicio <= DATEADD(DAY,1,CONVERT(DATE,%7,103))
--and tabla.Ruta_Almacenamiento = 1
--and tabla.Responsable_del_Tramite =
-- and tabla.Ruta_Almacenamiento = 1


---------------------------------------------------------------------------------------------------------v2


DECLARE @mapID BIGINT= 355715 --ID del mapa de WF Correspondencia Recibida Fisica, cambiar en PRD
DECLARE @defID BIGINT= 329440 --ID de la categoría Comunicaciones Recibidas Fisicas, cambiar en PRD
select Workflow.SubWork_WorkID 'ID_del_Workflow',
(case when Attributes.WF_ValInt is null then 0 else 1 end) 'Ruta_Almacenamiento',
Attributes2.WF_ValInt 'Responsable_del_Tramite',
--Workflow.SubWork_Title, --borrar
--Workflow.SubWork_MapID Mapa, --borrar
Workflow.SubWork_DateInitiated 'Fecha_Inicio',
--Data.Data_UserData, --borrar
--Document.DataID, --borrar
AttributesCat1.ValStr 'Numero_de_Radicado',
AttributesCat2.ValDate 'Fecha_de_Radicado',
AttributesCat3.ValStr 'Dependencia_Competente'
from WMap Map
inner join WSubWork Workflow
on Workflow.SubWork_MapID = Map.Map_MapID
inner join WData Data
on Data.Data_WorkID = Workflow.SubWork_WorkID
inner join WFAttrData Attributes
on Attributes.WF_ID = Workflow.SubWork_WorkID
inner join WFAttrData Attributes2
on Attributes2.WF_ID = Workflow.SubWork_WorkID
inner join DTree Document
on Document.ParentID = Data.Data_UserData and Document.SubType = 144 and Document.Deleted = 0
inner join LLAttrData AttributesCat1
on AttributesCat1.ID = Document.DataID
inner join LLAttrData AttributesCat2
on AttributesCat2.ID = Document.DataID and AttributesCat2.VerNum = AttributesCat1.VerNum
inner join LLAttrData AttributesCat3
on AttributesCat3.ID = Document.DataID and AttributesCat3.VerNum = AttributesCat1.VerNum
where Map.Map_MapObjID = @mapID
and Data.Data_Type = 1
and Data.Data_SubType = 1
and Workflow.SubWork_Status = 1
and Attributes.WF_AttrType = 305
and Attributes2.WF_AttrType = 14
and AttributesCat1.DefID = @defID
and AttributesCat1.AttrID = 3
and AttributesCat2.DefID = @defID
and AttributesCat2.AttrID = 4
and AttributesCat3.DefID = @defID
and AttributesCat3.AttrID = 22
and AttributesCat1.VerNum = (select MAX(VerNum) from LLAttrData where ID = AttributesCat1.ID)
--filtros
--and AttributesCat1.ValStr = 'CR-BAQ00000001' --Numero_de_Radicado
--and AttributesCat3.ValStr = 'Dirección Nacional Fruver' --Dependencia_Competente
--and AttributesCat2.ValDate >= CONVERT(datetime,'2019-04-03') --Fecha_de_Radicado
--and AttributesCat2.ValDate < DATEADD(DAY,1,CONVERT(DATE,'2019-04-03')) --Fecha_de_Radicado
--and Workflow.SubWork_DateInitiated >= CONVERT(DATE,%6,103) --Fecha_Inicio
--and Workflow.SubWork_DateInitiated < DATEADD(DAY,1,CONVERT(DATE,%7,103)) --Fecha_Inicio
--and (case when Attributes.WF_ValInt is null then 0 else 1 end) = 0 --Ruta_Almacenamiento
--and Attributes2.WF_ValInt = 135177 --Responsable_del_Tramite
