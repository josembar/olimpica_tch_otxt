
select 
Usuario.ID_CONTENT,
RadicadoAsignadoUsuario.NOMBRE_RADICADO Radicado,
--RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO,
--RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO,
--RadicadoGenerado.ID_RAD_GENERADO,
SegmentoReal.NOMBRE Segmento
from CO_USUARIO Usuario
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignadoUsuario
on RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA = Usuario.ID_CONTENT
inner join (select RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO,
RadicadoGenerado.ID_RAD_GENERADO,
ROW_NUMBER() OVER(PARTITION BY RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO ORDER BY RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO) ROW
from CO_RADICADO_GENERADO RadicadoGenerado
where exists(select ID_RADICADO_ASIGNADO_USUARIO
from CO_RADICADO_GENERADO
where ID_RADICADO_ASIGNADO_USUARIO = RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO)) RadicadoGenerado
on RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO = RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO
inner join CO_SEGMENTO_GENERADO_VALOR SegmentoGenerado
on SegmentoGenerado.ID_RAD_GENERADO = RadicadoGenerado.ID_RAD_GENERADO
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentoGenerado.ID_SEG_REAL
where RadicadoAsignadoUsuario.ESTADO = 1
and RadicadoGenerado.ROW = 1
--and Usuario.ID_CONTENT = 86916
order by Usuario.ID_CONTENT


-----------------------------------------------------------------------



select RadicadoAsignado.ID_US_CORRESPONDENCIA USUARIO,
Radicado.NOMBRE_RADICADO RADICADO,
--Radicado.ID_FORMULA_IMPRIMIBLE,
--SegmentosAsignados.ID_FORMULA,
--SegmentosAsignados.ID_SEG_REAL,
SegmentoReal.NOMBRE SEGMENTO
from 
CO_RADICADO Radicado
inner join (
select ID_FORMULA,
ID_SEG_REAL from
CO_SEGMENTO_ASIGNADO_FECHA
union
select ID_FORMULA,
ID_SEG_REAL from
CO_SEGMENTO_ASIGNADO_VALOR
union
select ID_FORMULA,
ID_SEG_REAL from
CO_SEGMENTO_ASIGNADO_LISTA) SegmentosAsignados
on SegmentosAsignados.ID_FORMULA = Radicado.ID_FORMULA_IMPRIMIBLE
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentosAsignados.ID_SEG_REAL
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignado 
on RadicadoAsignado.NOMBRE_RADICADO = Radicado.NOMBRE_RADICADO
inner join CO_USUARIO Usuario
on Usuario.ID_CONTENT = RadicadoAsignado.ID_US_CORRESPONDENCIA
where RadicadoAsignado.ESTADO = 1
--and Usuario.ID_CONTENT = 86916
order by 1,2