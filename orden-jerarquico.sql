
/**************************************/



with Docs ([DataID],[Name],[Level],[SubType],[Sort],[Name2])
		as
		(
			select 
				(case p.SubType 
				when 848 then -p.DataID 
				else p.DataID 
                end) as DataID,
				p.Name,
				0 as [Level],
				p.SubType,
				convert(nvarchar(1024), p.Name), 
				convert(nvarchar(1024), p.Name) Name2
			from 
				csadmin.DTree p
			where p.DataID = 179041 -- DataID --
				and p.Deleted = 0
			union all
			select 
				c.DataID,
				c.Name,
				[Level] + 1,
				c.SubType,
				convert (nvarchar(1024), RTRIM(Sort) + '________' + d.Name + c.Name ),
				convert(nvarchar(1024), REPLICATE ('________' , d.[Level] +1) + c.Name)
			from 
				[csadmin].[DTree] c ,Docs d 
		    where d.DataID =  c.ParentID 
		          and c.Deleted = 0

		)
		select * from Docs
		order by Sort


        select abs(DTree.DataID) 'ID del Objeto', DTree.Name 'Nombre del Objeto/Elemento',
DTreeACL.RightID 'ID del Usuario', KUAF.Name 'Nombre del Usuario/Grupo', 
DTreeACL.Permissions, KUAF.Type,
DTree.SubType, 
DTree.DataID
from DTreeACL,DTree,KUAF
where DTree.DataID = DTreeACL.DataID
and DTreeACL.RightID = KUAF.ID
and DTree.SubType in('0','144','849')
~2
order by DTree.Name,DTree.ParentID