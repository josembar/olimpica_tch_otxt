declare @String nvarchar(255)
declare @size int
declare @count int
set @String = '6304013'
set @size = 10
set @count = (select LEN(@String))

while (@count) < @size
begin
	select @String = (select CONCAT('0',@String))
	select @count = @count+1
end

select @String Codigo, LEN(@String) Size