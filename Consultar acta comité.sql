select Documento.DataID DataID,
ActaNumero.ValStr ActaNumero,
GerenteComercial.ValInt GerenteComercial,
Estado.ValStr Estado
from DTree Documento
inner join LLAttrData ActaNumero
on ActaNumero.ID = Documento.DataID
inner join CatRegionMap NombreCategoria
on NombreCategoria.CatID = ActaNumero.DefID and RIGHT(NombreCategoria.RegionName,1) = ActaNumero.AttrID
inner join LLAttrData GerenteComercial
on GerenteComercial.ID = ActaNumero.ID
inner join CatRegionMap NombreCategoria2
on NombreCategoria2.CatID = GerenteComercial.DefID and RIGHT(NombreCategoria2.RegionName,1) = GerenteComercial.AttrID
inner join LLAttrData Estado
on Estado.ID = ActaNumero.ID
inner join CatRegionMap NombreCategoria3
on NombreCategoria3.CatID = Estado.DefID and RIGHT(NombreCategoria3.RegionName,1) = Estado.AttrID
where Documento.SubType = 144
and Documento.Deleted = 0
and NombreCategoria.CatName = 'Documentos acta de comité de codificación'
and NombreCategoria2.CatName = NombreCategoria.CatName
and NombreCategoria3.CatName = NombreCategoria.CatName
and NombreCategoria.AttrName = 'Acta número'
and NombreCategoria2.AttrName = 'Gerente Comercial'
and NombreCategoria3.AttrName = 'Estado'
and ActaNumero.VerNum = (select MAX(VerNum) from LLAttrData where ID = Documento.DataID)
and GerenteComercial.VerNum = ActaNumero.VerNum
and Estado.VerNum = ActaNumero.VerNum
and ActaNumero.ValStr = '00090'