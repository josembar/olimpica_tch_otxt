DECLARE @NRadicado NVARCHAR(MAX);

SELECT @NRadicado = N_Radicado
FROM Z_Radicado
WHERE LEFT(N_Radicado,4) = CONVERT(NVARCHAR(4),YEAR(getdate())) AND
Descripcion = %1
-- and Usado = 0
ORDER BY Seq DESC

IF @NRadicado IS NULL
SELECT CONVERT(NVARCHAR(4),YEAR(getdate()))+'-'+CONVERT(NVARCHAR(MAX),(COUNT(*) + 1)) AS 'Número de Radicado'
FROM Z_Radicado
WHERE LEFT(N_Radicado,4) = CONVERT(NVARCHAR(4),YEAR(getdate())) AND Descripcion = %1

ELSE IF @NRadicado IS NOT NULL
SELECT CONVERT(NVARCHAR(4),YEAR(getdate()))+'-'+CONVERT(NVARCHAR(MAX),(COUNT(*) + 1)) AS 'Número de Radicado'
FROM Z_Radicado
WHERE LEFT(N_Radicado,4) = CONVERT(NVARCHAR(4),YEAR(getdate())) AND Descripcion = %1

ELSE
SELECT @NRadicado AS 'Número de Radicado'