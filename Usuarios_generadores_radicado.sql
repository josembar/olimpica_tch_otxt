

select Usuario.ID_CONTENT,
RadicadoAsignadoUsuario.NOMBRE_RADICADO,
--RadicadoGenerado.ID_RAD_GENERADO,
--SegmentoGenerado.ID_SEG_REAL,
SegmentoReal.NOMBRE
from CO_USUARIO Usuario
inner join CO_RADICADO_ASIGNADO_USUARIO RadicadoAsignadoUsuario
on RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA = Usuario.ID_CONTENT
inner join CO_RADICADO_GENERADO RadicadoGenerado
on RadicadoGenerado.ID_RADICADO_ASIGNADO_USUARIO = RadicadoAsignadoUsuario.ID_RADICADO_ASIGNADO_USUARIO
inner join CO_SEGMENTO_GENERADO_VALOR SegmentoGenerado
on SegmentoGenerado.ID_RAD_GENERADO = RadicadoGenerado.ID_RAD_GENERADO
inner join CO_SEGMENTO_REAL SegmentoReal
on SegmentoReal.ID_SEG_REAL = SegmentoGenerado.ID_SEG_REAL
where RadicadoAsignadoUsuario.ESTADO = 1
and RadicadoAsignadoUsuario.ID_US_CORRESPONDENCIA = 86916
--and RadicadoAsignadoUsuario.NOMBRE_RADICADO = 'FRBAQ'


--select * from CO_RADICADO_ASIGNADO_USUARIO where ID_US_CORRESPONDENCIA = 86916 and ESTADO = 1

