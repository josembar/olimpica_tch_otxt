
select CodigoSAP.ValInt CODIGOSAP,
NombreCompleto.ValStr NOMBRECOMPLETO,
NIT.ValStr NIT,
EstadoProveedor.ValStr ESTADO
from LLAttrData CodigoSAP
inner join DTree BWS
on BWS.DataID = CodigoSAP.ID
inner join CatRegionMap Categoria1
on Categoria1.CatID = CodigoSAP.DefID and RIGHT(Categoria1.RegionName,1) = CodigoSAP.AttrID
inner join LLAttrData NombreCompleto
on NombreCompleto.ID = CodigoSAP.ID 
inner join CatRegionMap Categoria2
on Categoria2.CatID = NombreCompleto.DefID and RIGHT(Categoria2.RegionName,1) = NombreCompleto.AttrID
inner join LLAttrData NIT
on NIT.ID = CodigoSAP.ID
inner join CatRegionMap Categoria3
on Categoria3.CatID = NIT.DefID and RIGHT(Categoria3.RegionName,1) = NIT.AttrID
inner join LLAttrData EstadoProveedor
on EstadoProveedor.ID = CodigoSAP.ID
inner join CatRegionMap Categoria4
on Categoria4.CatID = EstadoProveedor.DefID and RIGHT(Categoria4.RegionName,1) = EstadoProveedor.AttrID
where Categoria1.CatName = 'Proveedor'
and Categoria2.CatName = Categoria1.CatName
and Categoria3.CatName = Categoria1.CatName
and Categoria4.CatName = Categoria1.CatName
and Categoria1.AttrName = 'Código SAP'
and Categoria2.AttrName = 'Nombre proveedor'
and Categoria3.AttrName = 'NIT'
and Categoria4.AttrName = 'Estado proveedor'
and BWS.Deleted = 0
and CodigoSAP.ValInt = 100163
