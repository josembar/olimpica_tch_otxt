

//función que reemplaza la función delRowSubmit( fieldName, msg ) que es una función por defecto de los formularios
//está función va en la propiedad onclick de los "botones" (enlaces) para borrar segmentos
function borrarCampoSegmento(fieldName,msg)
{
    if (confirm(msg))
    {
        var segmentos = obtenerSegmentos())
        if(segmentos)
        {
            //console.log(segmentos)
            segmentos.splice(parseInt(fieldName[fieldName.length - 1]) - 1,1)
            //console.log('elemento eliminado: '+segmentos)
            borrarSegmentos()
            sessionStorage.setItem("segmentos",segmentos)
            //las siguientes 3 líneas de código fueron extraídas de la función delRowSubmit( fieldName, msg )
            document.myForm.func.value = 'webform.AttrValueDelete';
            document.myForm.LL_AttrFieldName.value = fieldName;
            document.myForm.submit();
        }
    }
}


borrarCampo( '_1_1_3_1', '¿Suprimir este valor?')